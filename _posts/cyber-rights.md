# Cyber Rights

```
Fixing the broken links on https://www.activism.net/cyber/ 
capture from 2020-01-18: https://web.archive.org/web/20200118051910/https://www.activism.net/cyber/

- Site was maintained until 2004 – I want to link to a Wayback Machine capture from that year, and if it's still around, the current iteration of the page too
```



---

# Activism: Cyber Rights and Online Civil Liberties

---

See also: [Government](https://web.archive.org/web/20200118052535/https://www.activism.net/government/), [Privacy](https://web.archive.org/web/20200118052815/https://www.activism.net/privacy/), [Ban Spam](https://web.archive.org/web/20200118052933/https://www.activism.net/privacy/banspam.shtml) and [Big Brother](https://web.archive.org/web/20200118052829/https://www.activism.net/privacy/bbrother.shtml)

---

Never mind, I'm an idiot…here's a capture of the entire page from August 2004: https://web.archive.org/web/20040803113320/http://www.activism.net/cyber/

